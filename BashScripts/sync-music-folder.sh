#!/bin/bash

# This script syncronizes the music folder from my server to my desktop computer.
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Don't run as root!
[ "$(whoami)" == "root" ] && {
	echo "Please run it as normal user!"
	exit 1
}

user="$(whoami)"
music_folder="/media/myghi-server/$user/Músicas/FLAC/"
destination_folder="/home/$user/Músicas/"

# Check if the folder is accessible (autofs)
if [ -d "$music_folder" ]
then
	# Sync files with some exceptions while logging
	rsync -r --delete --verbose --size-only "$music_folder" "$destination_folder"
	echo "Syncronized succesfully!"
else
	echo "Music folder not accessible!"	
fi
