#!/bin/bash
# Simple script to log all starts and restarts from my minecraft server
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# It's recommended to use both preallocated and max at the same size for performance.
# Don't use more than 12GB and never use the same size as your host OS,
# because java itself will use this + 1.5GB of ram, and your OS will need
# at least 1GB to work properly too!

preallocated_memory="4G"
max_memory="4G"

# This is where java runtime binary is. This argument allows you to use different java
# versions for different minecraft servers on your host.
java_runtime="/usr/lib/jvm/java-18-openjdk/bin/java"

# That's the java file to use
jarfile="pufferfish-paperclip-1.19-R0.1-SNAPSHOT-reobf.jar"

# Here are the java flags for improving performance. Using aikar flags by default.
java_flags="-Dterminal.jline=false -Dterminal.ansi=true -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:InitiatingHeapOccupancyPercent=15 --add-modules=jdk.incubator.vector"

while true ; do
	echo -e '\nStarted at '$(date "+%d-%b-%H:%M")'' >> script-log
	${java_runtime} -Xms${preallocated_memory} -Xmx${max_memory} $java_flags -jar $jarfile nogui
	echo -e '\nCrashed at '$(date "+%d-%b-%H:%M")'' >> script-log
done