#!/usr/bin/env bash

# Auxiliar na criação ou remoção de pastas do servidor.
# Copyright (C) 2024 - Myghi63 (myghi63@disroot.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Diretório de armazenamento do servidor
diretorioArmazenamento="/mnt/arquivos"

[[ $(whoami) == "root" ]] || {
	echo -e "\nÉ necessário ser superusuário para rodar esse script!"
	exit 1
}

clear

Confirma() {
	printf "\nTecle enter para continuar, ou Ctrl + C para cancelar..."
	read -r
	clear
}

Info() {
	printf "Para criar:\n\n"
	if [ "$arquivosHistorico" == "sim" ]
	then
		printf "%s\n" \
		"Pasta Raiz: $diretorioArmazenamento/$nomeDoCompartilhamento, CHOWN: root:$nomeDoGrupo, CHMOD: 0750" \
		"Pasta Arquivos: $diretorioArmazenamento/$nomeDoCompartilhamento/Arquivos, CHOWN: root:$nomeDoGrupow, CHMOD: 2775" \
		"Pasta Histórico: $diretorioArmazenamento/$nomeDoCompartilhamento/Histórico, CHOWN: root:root, CHMOD: 2755"
		[ -z "$gid" ] || printf "GID: %s -> %s, %s -> %s\n" "$nomeDoGrupo" "$gid" "$nomeDoGrupow" "$gidw"
	else
		if [ "$permissaoPastaSimples" == "sim" ]
		then
			chmod="2775"
		else
			chmod="2770"
		fi
		printf "Pasta Raiz: %s/%s, CHOWN: root:%s, CHMOD: %s\n" "$diretorioArmazenamento" "$nomeDoCompartilhamento" "$nomeDoGrupo" "$chmod"
		[ -z "$gid" ] || printf "GID: %s -> %s\n" "$nomeDoGrupo" "$gid"
	fi
}

DefineGID() {
	local statusGID="1"
	while [ "$statusGID" != "0" ]
	do
		read -p "Defina o GID para o grupo $1: " -r gidDefinida

		if [ -z "$gidDefinida" ]
		then
			clear
			statusGID="0"
			printf "GID para o grupo será decidido automaticamente! \n\n"
		elif [[ $gidDefinida =~ ^[0-9]+$ ]] && [ "$gidDefinida" -ge 0 ] && [ "$gidDefinida" -le 65535 ]
			then
				if [[ "$(getent group "$gidDefinida")" || "$gid" == "$gidDefinida" ]]
				then
					clear
					printf "GID já pertence à outro grupo! \n\n"
				else
					clear
					printf "GID será utilizada para a criação do grupo.\n\n"
					statusGID="0"
				fi
			else
				clear
				printf "\nGID Inválido! \n\n"
			fi
	done
}

CriaPasta() {
	# Criação de Pastas
	mkdir "$diretorioArmazenamento/$nomeDoCompartilhamento"
	[ "$arquivosHistorico" == "sim" ] && {
		mkdir "$diretorioArmazenamento/$nomeDoCompartilhamento/Arquivos"
		mkdir "$diretorioArmazenamento/$nomeDoCompartilhamento/Histórico"
	}

	# Aplicar permissões
	if [ "$arquivosHistorico" == "sim" ]
	then
		chmod 0750 "$diretorioArmazenamento/$nomeDoCompartilhamento"
		chmod 2775 "$diretorioArmazenamento/$nomeDoCompartilhamento/Arquivos"
		chmod 2755 "$diretorioArmazenamento/$nomeDoCompartilhamento/Histórico"
	else
		if [ "$permissaoPastaSimples" == "sim" ]
		then
			chmod 2775 "$diretorioArmazenamento/$nomeDoCompartilhamento"
		else
			chmod 2770 "$diretorioArmazenamento/$nomeDoCompartilhamento"
		fi
	fi
}

CriaGrupos() {
	# Criação de Grupos
	if [ "$gid" != "" ]
	then
		groupadd -r "$nomeDoGrupo" -g "$gid"
		[ "$arquivosHistorico" == "sim" ] && groupadd -r "$nomeDoGrupow" -g "$gidw"
	else
		groupadd -r "$nomeDoGrupo"
		[ "$arquivosHistorico" == "sim" ] && groupadd -r "$nomeDoGrupow"
	fi

	# Aplicar grupos
	chown root:"$nomeDoGrupo" "$diretorioArmazenamento/$nomeDoCompartilhamento"
	[ "$arquivosHistorico" == "sim" ] && {
		chown root:"$nomeDoGrupow" "$diretorioArmazenamento/$nomeDoCompartilhamento/Arquivos"
		chown root:root "$diretorioArmazenamento/$nomeDoCompartilhamento/Histórico"
	}
}

ExcluiPasta() {
		groupdel "$nomeDoGrupo"
		[ -d "$diretorioArmazenamento/$nomeDoCompartilhamento/Arquivos" ] && groupdel "$nomeDoGrupo""w"
		rm -rf "${diretorioArmazenamento:?}"/"${nomeDoCompartilhamento}"
}

DefinirGID() {
	DefineGID "$nomeDoGrupo"
	gid="$gidDefinida"
	[[ "$gid" != "" && "$arquivosHistorico" == "sim" ]] && {
		DefineGID "$nomeDoGrupow"
		gidw="$gidDefinida"
	}
}

CriarPasta() {
	read -p "A pasta deverá seguir a estrutura de Arquivos + Histórico? [sim/NÃO]: " -r arquivosHistorico
	clear

	[ "$arquivosHistorico" != "sim" ] && {
		read -p "A pasta deverá permitir acesso de leitura para quem não estiver no grupo? [sim/NÃO]: " -r permissaoPastaSimples
	}

	clear
	DefinirGID
	Info
	Confirma
	CriaPasta
	CriaGrupos
	printf "Pastas e grupos criados!\n\n"
	ls -lar "$diretorioArmazenamento/$nomeDoCompartilhamento"
}

ExcluirPasta() {
	printf "Você deseja excluir %s? Para confirmar, digite SIM (caixa alta): " "$nomeDoCompartilhamento"
	read -r excluir

	[ "$excluir" == "SIM" ] && {
		Confirma
		ExcluiPasta
		printf "Pastas e grupos removidos!\n\n"
		ls -lar "$diretorioArmazenamento"
	}
}

# Início do Programa
read -p "Digite o nome da pasta de compartilhamento: " -r nomeDoCompartilhamento
clear

# Cancelar se usuário não tiver inserido o nome
[ -z "$nomeDoCompartilhamento" ] && exit 1

# Definir nome do grupo das pastas automaticamente
nomeDoGrupo=${nomeDoCompartilhamento,,}
nomeDoGrupow="$nomeDoGrupo""w"

# Verificar existência da pasta e decidir o que fazer
if [ -d "$diretorioArmazenamento/$nomeDoCompartilhamento" ]
then
	ExcluirPasta
else
	CriarPasta
fi
