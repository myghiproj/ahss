#!/bin/bash

set -e

projectURL="https://github.com/MekhyW/COOKIEBOT-backend"
projectFolder="COOKIEBOT-backend"
serverFolder="/javaserver/COOKIEBOT-backend"
serverUser="javaserver"
serverGroup="javaserver"
systemdService="javaserver@COOKIEBOT-backend.service"

rm -rf "$projectFolder"/ ||: &> /dev/null

git clone "$projectURL"

mkdir "$projectFolder"/src/main/resources/
cp application.properties "$projectFolder"/src/main/resources/application.properties

cd "$projectFolder"

sudo systemctl stop "$systemdService"
mvn package

sudo rm "$serverFolder"/server.jar
sudo mv target/*.jar "$serverFolder"/server.jar
sudo chown "$serverUser":"$serverGroup" "$serverFolder"/server.jar

sudo systemctl start "$systemdService"

cd ..
rm -rf "$projectFolder"/