#!/bin/bash

# This script takes any image named "cover" and convert to .png at 700x700 in a optimized way.
# Depends on optipng, imagemagick and pngquant.
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Abort if there's a error
set -e

# Resize image, remove alpha layer and turn it 8 bit
convert cover.* -resize 700x700 -alpha off -depth 8 processing.png

# Optimize png file
pngquant processing.png
optipng processing-fs8.png

# Remove old files and rename to cover.png
rm cover.* processing.png
mv processing-fs8.png cover.png
