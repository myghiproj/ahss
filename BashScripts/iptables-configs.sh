#!/bin/bash

# This script is meant to protect any linux server using iptables as firewall. Don't forget to enable iptables and ip6tables services before!
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Abort if there's a error
set -e

# Run the script with administrative privileges
sudo -s <<EOF

    # Drop connection of some unused things
    iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
    iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
    iptables -t mangle -A PREROUTING -s 224.0.0.0/3 -j DROP 
    iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP 
    iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP 
    iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP 
    iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP 
    iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP 
    iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP 
    iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP

    # Limit connections at once
    iptables -A INPUT -p tcp -m connlimit --connlimit-above 80 -j REJECT --reject-with tcp-reset
    iptables -A INPUT -p tcp --tcp-flags RST RST -m limit --limit 2/s --limit-burst 2 -j ACCEPT 
    iptables -A INPUT -p tcp --tcp-flags RST RST -j DROP
    iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m limit --limit 60/s --limit-burst 20 -j ACCEPT 
    iptables -A INPUT -p tcp -m conntrack --ctstate NEW -j DROP

    # Avoid fragmentations
    iptables -t mangle -A PREROUTING -f -j DROP

    # Avoid invalid connections
    iptables -A INPUT -p tcp -m tcp -m conntrack --ctstate INVALID,UNTRACKED -j SYNPROXY --sack-perm --timestamp --wscale 7 --mss 1460 
    iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

    # Protect ssh
    iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --set 
    iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --update --seconds 60 --hitcount 10 -j DROP  

    # Block port scanning
    iptables -N port-scanning 
    iptables -A port-scanning -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s --limit-burst 2 -j RETURN 
    iptables -A port-scanning -j DROP

    # Save configuration
    iptables-save -f /etc/iptables/iptables.rules

EOF

echo "Settings applied and saved!"
