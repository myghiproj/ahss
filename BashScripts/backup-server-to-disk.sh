#!/bin/bash

# This script makes a partial server backup to my external hard drive.
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

# Configurations
date=`date "+%d-%b-%y-%R"`
server_directory="/mnt/myghi-server"
backup_disk="/mnt/HD-4TB/backup/"
logfile="$backup_disk/last-backup"

# Don't run as root
[ "$(whoami)" == "root" ] && {
	echo "Please run it as normal user!"
	exit 1
}

# Check if the folder is accessible
[ -z "$(ls $backup_disk)" ] && { 
	echo "Backup disk not mounted!"
	exit 1
}

# Start log file
echo -e "Backup done at $date" > "$logfile"
		
# Sync files with some exceptions while logging
for user in psergio adriana myghi63
do
	rsync -rltv --chmod=ug+rwX,o+rX --delete --log-file="$logfile" --exclude={".*","Histórico"} "$server_directory/$user/" "$backup_disk/$user/"
done
