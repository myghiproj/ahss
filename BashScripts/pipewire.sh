#!/bin/bash

# This script is made to configure and optimize pipewire for almost all use cases.
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Set the desired resample quality here.
# 0 = worse, 10 = good, 15 = best (but 2~3x more CPU usage)
audio_quality="10"

# Tell the user what to do if this script won't work properly
echo -e "\nIn order to all these settings work, please check if you have rtkit, wireplumber, pipewire, pipewire-pulse, pipewire-jack and pipewire-alsa installed on your system. Contents will be copied from /usr/share/pipewire to /etc/pipewire and then edited.\n"

# Run this script as root
sudo -s <<EOF

	# Stop if something goes wrong
	set -e 

	# Create necessary folders if needed
	test -d /etc/pipewire/ || mkdir /etc/pipewire/
	test -d /etc/wireplumber/ || mkdir /etc/wireplumber/

	# /etc/pipewire/minimal.conf
	sed -e '/nice.level/s/=.*/=\ -15/g' -e '/audio.format/s/=.*/=\ \"S16\"/g' -e '/audio.format/s/#//g' -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' /usr/share/pipewire/minimal.conf > /etc/pipewire/minimal.conf

	# /etc/pipewire/client.conf
	sed -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' /usr/share/pipewire/client.conf > /etc/pipewire/client.conf

	# /etc/pipewire/client-rt.conf
	sed -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' /usr/share/pipewire/client-rt.conf > /etc/pipewire/client-rt.conf

	# /etc/pipewire/pipewire.conf
	sed -e '/nice.level/s/=.*/=\ -15/g' /usr/share/pipewire/pipewire.conf > /etc/pipewire/pipewire.conf

	# /etc/pipewire/pipewire-pulse.conf
	sed -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' -e '/nice.level/s/=.*/=\ -15/g' -e '/pulse.default.format/s/=.*/=\ S16/g' -e '/pulse.default.format/s/#//g' -e '/module-switch-on-connect/s/#//g' /usr/share/pipewire/pipewire-pulse.conf > /etc/pipewire/pipewire-pulse.conf

	# /etc/wireplumber/wireplumber.conf
	sed -e '/nice.level/s/=.*/=\ -15/g' /usr/share/wireplumber/wireplumber.conf > /etc/wireplumber/wireplumber.conf 

	# Setup permissions for folders and files, just in case if umask isn't 022
	find /etc/wireplumber/ -type d -print0 | xargs -0 sudo chmod 755
	find /etc/wireplumber/ -type f -print0 | xargs -0 sudo chmod 644
	find /etc/pipewire/ -type d -print0 | xargs -0 sudo chmod 755
	find /etc/pipewire/ -type f -print0 | xargs -0 sudo chmod 644

	echo -e "\nSettings applied! Please reboot!\n"

EOF
