#!/bin/bash

# Decrypt the drives and mount a RAID1 btrfs partition on my server
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

[ "$(whoami)" == "root" ] || { 
	echo -e "Please run as root!" 
	exit 1
}

read -p "Please enter the Passphrase: " -s passphrase
echo -e "\nDecrypting..."

echo -e "$passphrase" | cryptsetup luksOpen /dev/disk/by-uuid/17eabd12-a8a2-4cfa-a622-8a0779662f86 ARQUIVOS1
echo -e "$passphrase" | cryptsetup luksOpen /dev/disk/by-uuid/acc7be15-5c51-417c-90b5-0f08f46706b8 ARQUIVOS2

echo -e "Mounting..."
mount /mnt/arquivos
mount /srv/nfs4
exportfs -arv

echo -e "Decrypted and mounted successfully!"
