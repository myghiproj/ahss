#!/bin/bash

# This script installs and configure specific stuff for my usecase. Made to use after a AAIS install.
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e
user="$(whoami)"

# Packages from the official arch linux repo
official_packages="base-devel git lib32-libxslt gamemode lib32-gamemode wine-staging winetricks wine-gecko vkd3d lib32-vkd3d libreoffice-fresh-pt-br discord telegram-desktop steam qbittorrent obs-studio simplescreenrecorder audacity soundkonverter kid3 avidemux-qt handbrake gimp sox opencv swh-plugins gst-plugins-good lib32-gst-plugins-good gst-plugins-bad gst-plugins-ugly vorbis-tools vorbisgain hplip imagescan"

# Packages from AUR repo
aur_packages="dxvk-bin multimc5 timeshift anydesk-bin ttf-ms-win10-auto epson-inkjet-printer-escpr hplip-plugin imagescan-plugin-networkscan autofs"

[[ $(whoami) == "root" ]] && {
	echo -e "\nDon't run it as root!"
	exit 1
}

# Enable authentication without password
sudo sed -i '/'"$user"'/s/ALL=(ALL)\ ALL/ALL=(ALL)\ NOPASSWD:\ ALL/' /etc/sudoers

# Install official packages while making sure that the system is updated
args="--needed --noconfirm"
while [ "$official_install" != "finished" ]
do
	official_install="finished"
	sudo pacman -Syu $args $official_packages || {
		read -p "The installation failed. Do you want to try again? [y/N]: " choice
		if [ "$choice" == "y" ]
		then
			official_install="failed"
			args="--needed"
		else
			exit 1
		fi
	}
done

# Edit makepkg.conf to improve compiling performance for AUR packages
sudo sed -i -e '/PKGEXT/s/.zst//g' -e 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j$(nproc)"/' -e 's/^DEBUG_/#DEBUG_/' -e 's/zstd\ -c\ -z\ -q/zstd\ -c\ -z\ -q\ --threads=0/' /etc/makepkg.conf

# Clean packages from cache to free up space
sudo rm -rf /var/cache/pacman/pkg/*

# Install yay AUR helper
yay --version || { 
	git clone "https://aur.archlinux.org/yay-bin.git"
	cd yay-bin
	makepkg -si --noconfirm
	cd ..
	rm -rf yay-bin*
}

# Install AUR packages
args="--needed --noconfirm"
while [ "$installation" != "finished" ]
do
	installation="finished"
	yay -Syu $args $aur_packages || {
		read -p "The installation failed. Do you want to try again? [y/N]: " choice
		if [ "$choice" == "y" ]
		then
			installation="failed"
			args="--needed"
		fi
	}
done

# Remove unnecessary packages and clean cache again to free space
yay -Yc --noconfirm
sudo rm -rf /var/cache/pacman/pkg/*
sudo rm -rf /home/"$user"/.cache/yay*

# Disable authentication with password
sudo sed -i '/'"$user"'/s/NOPASSWD\:\ ALL/ALL/g' /etc/sudoers
