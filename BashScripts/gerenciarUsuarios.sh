#!/usr/bin/env bash

# Auxiliar na criação, remoção e gerenciamento de contas de usuários UNIX.
# Copyright (C) 2024 - Myghi63 (myghi63@disroot.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

[[ $(whoami) == "root" ]] || {
	echo -e "\nÉ necessário ser superusuário para rodar esse script!"
	exit 1
}

clear

Confirma() {
	printf "\nTecle enter para continuar, ou Ctrl + C para cancelar..."
	read -r
	clear
}

Info() {
	local gruposExibidos
	local idExibido
	local gidExibido
	local usuarioExistente="$1"

	if [ "$usuarioExistente" == "1" ]
	then
		gruposExibidos="$(id "$nomeDoUsuario" -G -n)"
		idExibido="$(id "$nomeDoUsuario" -u)"
		gidExibido="$(id "$nomeDoUsuario" -g)"
	else
		gruposExibidos="$grupos"
		idExibido="$idDoUsuario"
		gidExibido="$idDoUsuario"
	fi

	printf "%s\n" "Informações atuais do usuário:" \
	"" \
	"Usuário: $nomeDoUsuario"
	[ -z "$gruposExibidos" ] || printf "Grupos: %s\n" "$gruposExibidos"
	[ -z "$idExibido" ] || printf "ID do usuário: %s\n" "$idExibido"
	[ -z "$gidExibido" ] || printf "GID do usuário: %s\n" "$gidExibido"
	[ -z "$senha" ] || printf "Senha do usuário: %s\n" "$senha"
}

Edita() {
	printf "%s\n" "O que você deseja fazer?" \
        "" \
        "[0] Trocar Senha" \
        "[1] Adicionar Grupos" \
        "[2] Remover Grupos" \
        "[3] Excluir Usuário" \
	""

	read -p "Digite a opção desejada: " -r edita
	clear
}

CriaUsuario() {
	printf "Criando novo usuário...\n"
	if [ -z "$idDoUsuario" ]
	then 
		useradd -m "$nomeDoUsuario"
	else
		useradd -m "$nomeDoUsuario" -u "$idDoUsuario"
	fi
}

AplicaSenha() {
	printf "Aplicando senha...\n"
	echo -e "$senha\n$senha" | passwd "$nomeDoUsuario"
	echo -e "$senha\n$senha" | smbpasswd -a "$nomeDoUsuario"
}

DefineSenha() {
	local statusSenha="1"
	while [ "$statusSenha" != "0" ]
	do
		local senha1
		local senha2

		read -p "Digite a senha para o usuário: " -r -s senha1
		clear

		read -p "Digite a senha novamente: " -r -s senha2
		clear

		if [ -z "$senha1" ] && [ -z "$senha2" ]
		then
			printf "Senhas vazias não são permitidas! \n\n"
		elif [ "$senha1" == "$senha2" ]
		then
			senha="$senha1"
			statusSenha="0"
		else
			printf "As senhas não coincidem! \n\n"
		fi
	done
}

AdicionaGrupos() {
	printf "Adicionando usuário aos grupos...\n"
	for grupo in $grupos
	do
		gpasswd -a "$nomeDoUsuario" "$grupo"
	done
}

RemoveGrupos() {
	printf "Removendo usuário aos grupos...\n"
	for grupo in $grupos
	do
		gpasswd -d "$nomeDoUsuario" "$grupo"
	done
}

DefineGrupos() {
	read -p "Digite os grupos, separados por espaço: " -r grupos
	clear
	if [ -z "$grupos" ]
	then
		printf "Nenhum grupo será gerenciado.\n"
	else
		printf "Grupos: %s\n" "$grupos"
	fi
}

DefineID() {
	local statusID="1"
	while [ "$statusID" != "0" ]
	do
		read -p "Defina o ID para o usuário (Vazio = Automático): " -r idDoUsuario

		if [ -z "$idDoUsuario" ] 
		then
			clear
			statusID="0"
			printf "ID para o usuário será decidida automaticamente! \n\n"
		elif [[ $idDoUsuario =~ ^[0-9]+$ ]] && [ "$idDoUsuario" -ge 0 ] && [ "$idDoUsuario" -le 65535 ]
			then
				clear
				if [ "$(id "$idDoUsuario")" ]
				then
					clear
					printf "ID já pertence à outro usuário! \n\n"
				else
					clear
					printf "ID será utilizada para a criação do usuário.\n\n"
					statusID="0"
				fi
			else
				clear
				printf "ID Inválido! \n\n"
			fi
	done
}

RemoveUsuario() {
	printf "Removendo Usuário...\n\n"
	smbpasswd -x "$nomeDoUsuario"
	userdel "$nomeDoUsuario"
}

TrocarSenha() {
	DefineSenha
	Confirma
	AplicaSenha
	status="0"
	Info "1"
}

AdicionarGrupos() {
	DefineGrupos
	[ -z "$grupos" ] || {
		Confirma
		AdicionaGrupos
		status="0"
		clear
		Info "1"
	}
}

RemoverGrupos() {
	DefineGrupos
	[ -z "$grupos" ] || {
		Confirma
		RemoveGrupos
		status="0"
		clear
		Info "1"
	}
}

RemoverUsuario() {
	printf "O usuário %s será removido.\n" "$nomeDoUsuario"
	Confirma
	RemoveUsuario
	status="0"
}

CriarUsuario() {
	clear
	DefineID
	DefineSenha
	DefineGrupos
	clear
	Info "0"
	Confirma
	CriaUsuario
	AplicaSenha
	[ -z "$grupos" ] || AdicionaGrupos
	clear
	[ "$(id "$nomeDoUsuario")" ] && Info "1"
}

# Início do Programa
printf "Digite o nome do usuário: "
read -r nomeDoUsuario
clear

# Cancelar se usuário não tiver inserido o nome
[ -z "$nomeDoUsuario" ] && exit 1

if [ "$(id "$nomeDoUsuario")" ]
then
	Info "1"
	Confirma
	
	status="1"
	while [ "$status" != "0" ]
	do
		Edita
		case $edita in
			"0")
				TrocarSenha
				;;
			"1")
				AdicionarGrupos
				;;
			"2")
				RemoverGrupos
				;;
			"3")
				RemoverUsuario
				;;
			*)
				# Inválido
				;;
		esac
	done
else
	CriarUsuario
fi

systemctl restart smb smbd
