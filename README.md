# Personal Scripts For Everything

This is a personal project containing lots of scripts for my everyday use. There will be some custom scripts, services and config files.

Some of these scripts needs to have a connection with my home network and server in order to work.

To install Arch Linux as a server or desktop OS, you can use AAIS for that: https://gitlab.com/myghiproj/aais

## License

GPLv3 is the license for all these scripts and you should receive a copy of it while cloning any project of mine.
